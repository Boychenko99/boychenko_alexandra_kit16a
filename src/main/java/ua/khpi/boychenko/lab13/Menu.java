package ua.khpi.boychenko.lab13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Макрокоманда (Шаблон Command); Колекція об'єктів Класу ConsoleCommand
 * 
 * @author Бойченко Олександра
 * @version 1.0
 * @See ConsoleCommand
 */
public class Menu implements Command {

	/**
	 * Колекція консольных команд;
	 * 
	 * @see ConsoleCommand
	 */
	private List<ConsoleCommand> menu = new ArrayList<ConsoleCommand>();

	/**
	 * Додає нову команду в колекцію
	 * 
	 * @Param command реалізує {@linkplain ConsoleCommand}
	 * @Return command
	 */
	public ConsoleCommand add(ConsoleCommand command) {
		menu.add(command);
		return command;
	}

	@Override
	public String toString() {
		String s = "Enter command...\n";
		for (ConsoleCommand c : menu) {
			s += c + ", ";
		}
		s += "'q'uit: ";
		return s;
	}

	@Override
	public void execute() {
		String s = null;
		String check = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		menu: while (true) {
			do {
				System.out.print(this);
				try {
					s = in.readLine();
				} catch (IOException e) {
					System.err.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			char key = s.charAt(0);
			if (key == 'q') {
				System.out.println("Exit");
				break menu;
			}
			for (ConsoleCommand c : menu) {
				if (s.charAt(0) == c.getKey()) {
					System.out.println("Input 'y' for confirm action or other symbol for cancel action");
					try {
						check = in.readLine();
					} catch (IOException e) {
						System.err.println("Error: " + e);
						System.exit(0);
					}
					if (check.charAt(0) == 'y') {
						c.execute();
						continue menu;
					} else {
						continue menu;
					}
				}
			}
		}
	}
}
