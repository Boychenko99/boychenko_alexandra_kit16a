package ua.khpi.boychenko.lab13;


import ua.khpi.boychenko.lab11.View;

/**
 * Консольна команда View; шаблон Command
 * 
 * @author Бойченко Олександра
 * @version 1.0
 */
public class ViewConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 *
	 */
	private View view;

	/**
	 * Ініціалізує поле {@linkplain ViewConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public ViewConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'v';
	}

	@Override
	public String toString() {
		return "'v'iew";
	}

	@Override
	public void execute() {
		System.out.println("View current.");
		view.viewShow();
	}
}
