package ua.khpi.boychenko.lab13;

/**
 * Обчислення і відображення Результатів; містить реалізацію Статичного методу
 * main ()
 * 
 * @Author Бойченко Олександра
 * @Version 4.0
 * @See Main # main
 */
public class Main {

	/**
	 * Виконується при запуску програми; Викликає метод {@linkplain Application # run ()}
	 * 
	 * @Param args параметри запуску програми
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run();
	}

}
