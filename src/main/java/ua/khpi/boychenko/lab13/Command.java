package ua.khpi.boychenko.lab13;

/**
 * Інтерфейс команди Або завдання; Шаблони: Command, Worker Thread
 * 
 * @Author Бойченко Олександра
 * @Version 1.0  
 */
public interface Command {

	/** Виконання команди; шаблони: Command, Worker Thread */
	public void execute();
}
