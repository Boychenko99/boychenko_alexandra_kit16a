package ua.khpi.boychenko.lab16;
import java.awt.Dimension;
import ua.khpi.boychenko.lab11.ViewResult;

/**
 * ConcreteProduct (шаблон проектування Factory Method); відображенна графіка
 * 
 * @author Бойченко Олександра
 * @version 1.0
 * @see ViewResult
 * @see Window
 */
public class ViewWindow extends ViewResult {

	/** Інформацію про елементів коллекци */
	private static final int POINTS_NUM = 100;
	/** Отображаемое окно */
	private Window window = null;

	/** Створення та відображення вікна */
	public ViewWindow() {
		super();
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Result");
		//window.setVisible(true);
	}

	@Override
	public void viewInit(int x) {
		Init(x);
	}

    @Override
    public void viewBody() {

    }

    @Override
    public void viewFooter() {

    }

    @Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}
