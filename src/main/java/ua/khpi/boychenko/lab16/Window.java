package ua.khpi.boychenko.lab16;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;

/**
 * Створення вікна відображення графіка
 * 
 * @author Бойченко Олександра
 * @version 1.0
 * @see Frame
 */
@SuppressWarnings("serial")
public class Window extends Frame {
	/** Товщина відступу від краю вікна */
	private static final int BORDER = 20;


	private ViewResult view;

	/**
	 * Ініціалізує {@linkplain Window # view}; <br>
	 * Створює обробник події закриття вікна
	 *
	 * @Param view Значення для поля {@linkplain Window # view}
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				setVisible(false);
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER;
		r.y += 25 + BORDER;
		r.width -= r.x + BORDER;
		r.height -= r.y + BORDER;
		c.x = r.x;
		c.y = r.y + r.height / 2;
		c.width = r.width;
		c.height = r.height / 2;
		g.setColor(Color.LIGHT_GRAY);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		int maxX = 0;
		double scaleX = 0;
		int maxY = 0;
		double scaleY = 0;
		for (Item item : view.getItems()) {
			if (item.getX() > maxX) {
				maxX = item.getX();
			}

			if (item.getY() > maxY)
				maxY = item.getY();
		}
		g.drawString("+" + maxY, r.x, r.y);
		g.drawString("+" + maxX, c.x + c.width - g.getFontMetrics().stringWidth("+" + maxX), c.y);
		scaleX = c.width / (int) maxX;
		scaleY = c.height / (int) maxY;
		g.setColor(Color.BLUE);
		for (Item item : view.getItems()) {
			g.drawOval(c.x + (item.getX() * (int) scaleX) - 5, c.y - (item.getY() * (int) scaleY) - 5, 10, 10);
		}
	}
}