package ua.khpi.boychenko.lab16;

import ua.khpi.boychenko.lab11.View;
import ua.khpi.boychenko.lab11.Viewable;

/** ConcreteCreator (Шаблон проектування Factory Method) <br>
 * Оголошує метод, "Фабрика", який створює об'єкти
 *
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See ViewableResult
 * @See ViewableWindow # getView ()
 */
public class ViewableWindow implements Viewable {
	@Override
	public View getView() {
		return new ViewWindow();
	}
}
