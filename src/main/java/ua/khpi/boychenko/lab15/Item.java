package ua.khpi.boychenko.lab15;
/**
 * Визначає елемент колекції
 * 
 * @Author Бойченко Олександра
 * @See Items  
 */
public class Item implements Comparable<Item> {

	/** Інформаційне поле */
	private String data;

	/**
	 * Ініціалізує {@linkplain Item # data}
	 *
	 * @Param data значення для поля {@linkplain Item # data}
	 */
	public Item(String data) {
		this.data = data;
	}

	/**
	 * Встановлює поле {@linkplain Item # data}
	 *
	 * @Param data Значення для поля {@linkplain Item # data}
	 * @Return значення поля {@linkplain Item # data}
	 */
	public String setData(String data) {
		return this.data = data;
	}

	/**
	 * Повертає поле {@linkplain Item # data}
	 *
	 * @Return значення поля {@linkplain Item # data}
	 */
	public String getData() {
		return data;
	}

	@Override
	public int compareTo(Item o) {
		return data.compareTo(o.data);
	}

	@Override
	public String toString() {
		return data;
	}
}
