package ua.khpi.boychenko.lab15;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Базовий клас спостерігача,   використовує анотації для визначення методів,
 * обробних деяких подій; шаблон Observer
 * 
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See Observer
 * @See Observable  
 */
public abstract class AnnotatedObserver implements Observer {

	/** Асоціативний масив обробників подій; містить пари подія-обробник */
	private Map<Object, Method> handlers = new HashMap<Object, Method>();

	/**
	 * Заповнює {@linkplain AnnotatedObserver # handlers} посиланнями на методи,
	 *   * Відмічені анотацією {@linkplain Event}  
	 */
	public AnnotatedObserver() {
		for (Method m : this.getClass().getMethods()) {
			if (m.isAnnotationPresent(Event.class)) {
				handlers.put(m.getAnnotation(Event.class).value(), m);
			}
		}
	}

	@Override
	public void handleEvent(Observable observable, Object event) {
		Method m = handlers.get(event);
		try {
			if (m != null)
				m.invoke(this, observable);
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
