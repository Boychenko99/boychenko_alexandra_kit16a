package ua.khpi.boychenko.lab15;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Контейнер об'єктів; наблюдаемый об'єкт; шаблон Observer
 * 
 * @author Бойченко Олександра
 * @see Observable
 * @see Observer
 * @see Item
 */
public class Items extends Observable implements Iterable<Item> {

	/** Константа-ідентифікатор події, оброблюваного спостерігачами */
	public static final String ITEMS_CHANGED = "ITEMS_CHANGED";
	/** Константа-ідентифікатор події, оброблюваного спостерігачами */
	public static final String ITEMS_EMPTY = "ITEMS_EMPTY";
	/** Константа-ідентифікатор події, оброблюваного спостерігачами */
	public static final String ITEMS_REMOVED = "ITEMS_REMOVED";

	/** Колекція об'єктів класу {@linkplain Item} */
	private List<Item> items = new ArrayList<Item>();

	/**
	 * Додає об'єкт в колекцію і сповіщає спостерігачів
	 *
	 * @Param item Об'єкт класу {@linkplain Item}
	 */
	public void add(Item item) {
		items.add(item);
		if (item.getData().isEmpty())
			call(ITEMS_EMPTY);
		else
			call(ITEMS_CHANGED);
	}

	/**
	 * Додає об'єкт в колекцію   * @Param s передається конструктору
	 * {@linkplain Item # Item (String)}  
	 */
	public void add(String s) {
		add(new Item(s));
	}

	/**
	 * Додає кілька об'єктів в колекцію і сповіщає спостерігачів   * @Param n
	 * кількість додаються об'єктів класу {@linkplain Item}  
	 */
	public void add(int n) {
		if (n > 0) {
			while (n-- > 0) {
				items.add(new Item(""));
				call(ITEMS_EMPTY);
			}
		}
	}

	/**
	 * Видаляє об'єкт з колекції і сповіщає спостерігачів
	 *
	 * @Param item Видаляється об'єкт
	 */
	public void del(Item item) {
		if (item != null) {
			items.remove(item);
			call(ITEMS_REMOVED);
		}
	}

	/**
	 * Видаляє об'єкт з колекції і сповіщає спостерігачів
	 *
	 * @Param index індекс об'єкт яий видаляється
	 */
	public void del(int index) {
		if ((index >= 0) && (index < items.size())) {
			items.remove(index);
			call(ITEMS_REMOVED);
		}
	}

	/**
	 * Повертає посилання на колекцію
	 *
	 * @Return посилання на колекцію об'єктів класу {@linkplain Item}
	 */
	public List<Item> getItems() {
		return items;
	}

	@Override
	public Iterator<Item> iterator() {
		return items.iterator();
	}
}
