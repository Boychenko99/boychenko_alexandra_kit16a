package ua.khpi.boychenko.lab14;


import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;
import ua.khpi.boychenko.lab13.Command;

/**
 * Завдання, використовувана Оброблювачем потоку; Шаблон Worker Thread
 * 
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See Command
 * @See CommandQueue
 */
public class MinCommand implements Command {

	/**
	 * Ініціалізує поле {@linkplain MinCommand # viewResult}
	 * 
	 * @Param viewResult об'єкт класу {@linkplain ViewResult}
	 */
	public MinCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/** Зберігає результат обробки колекції */
	private int result = 0;

	/** Прапор готовності результату */
	private int progress = 0;

	/**
	 * Обслуговує колекцію об'єктів
	 *
	 */
	private ViewResult viewResult;

	/**
	 * Повертає поле {@linkplain MinCommand # viewResult}
	 * 
	 * @Return значення {@linkplain MinCommand # viewResult}
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}

	/**
	 * Встановлює поле {@linkplain MinCommand # viewResult}
	 * 
	 * @Param viewResult значення для {@linkplain MinCommand # viewResult}
	 * @Return нове значення {@linkplain MinCommand # viewResult}
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}

	/**
	 * Повертає результат
	 * 
	 * @Return поле {@linkplain MinCommand # result}
	 */
	public int getResult() {
		return result;
	}

	/**
	 * Перевіряє готовність результату
	 * 
	 * @Return false - якщо результат знайдений, інакше - true
	 * @See MinCommand # result
	 */
	public boolean running() {
		return progress == 0;
	}

	@Override
	public void execute() {
		progress = 0;
		System.out.println("Find min value");
		result = 0;

		// int numberCounter=viewResult.getItems().size();
		int minResult = viewResult.getItems().get(0).getY();

		for (Item item : viewResult.getItems()) {

			if (minResult > item.getY())
				minResult = item.getY();

		}
		result = minResult;
		System.out.printf("Min result: %d\n", result);
		progress = 1;
	}
}
