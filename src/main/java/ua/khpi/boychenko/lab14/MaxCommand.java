package ua.khpi.boychenko.lab14;


import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;
import ua.khpi.boychenko.lab13.Command;

/**
 * Завдання, використовувана Оброблювачем потоку; Шаблон Worker Thread
 * 
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See Command
 * @See CommandQueue
 */
public class MaxCommand implements Command {

	/** Зберігає результат обробки колекції */
	private int result = 0;

	/** Прапор готовності результату */
	private int progress = 0;

	/**
	 * Обслуговує колекцію об'єктів

	 */
	private ViewResult viewResult;

	/**
	 * Ініціалізує поле {@linkplain MaxCommand # viewResult}
	 *
	 * @Param viewResult об'єкт класу {@linkplain ViewResult}
	 */
	public MaxCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * Повертає поле {@linkplain MaxCommand # viewResult}
	 *
	 * @Return значення {@linkplain MaxCommand # viewResult}
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}

	/**
	 * Встановлює поле {@linkplain MaxCommand # viewResult}
	 *
	 * @Param viewResult значення для {@linkplain MaxCommand # viewResult}
	 * @Return нове значення {@linkplain MaxCommand # viewResult}
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}

	/**
	 * Повертає результат
	 *
	 * @Return поле {@linkplain MaxCommand # result}
	 */
	public int getResult() {
		return result;
	}

	/**
	 * Перевіряє готовність результату
	 *
	 * @Return false - якщо результат знайдений, інакше - true
	 * @See MaxCommand # result
	 */
	public boolean running() {
		return progress == 0;
	}

	@Override
	public void execute() {
		progress = 0;
		System.out.println("Find max value");
		result = 0;

		// int numberCounter=viewResult.getItems().size();
		int maxResult = 0;

		for (Item item : viewResult.getItems()) {

			if (maxResult < item.getY())
				maxResult = item.getY();

		}
		result = maxResult;
		System.out.printf("Max result: %d\n", result);
		progress = 1;
	}
}
