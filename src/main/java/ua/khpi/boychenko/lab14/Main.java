package ua.khpi.boychenko.lab14;


import ua.khpi.boychenko.lab11.View;
import ua.khpi.boychenko.lab11.ViewableResult;
import ua.khpi.boychenko.lab13.*;

/**
 * Обчислення і відображення Результатів; містить реалізацію Статичного методу
 * main ()
 * 
 * @Author Бойченко Олександра
 * @Version 5.0
 * @See Main # main
 */
public class Main {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 */
	private View view = new ViewableResult().getView();

	/**
	 * Об'єкт класу {@linkplain Menu}; Макрокоманда (шаблон Command)
	 */
	private Menu menu = new Menu();

	/** Обробка команд користувача */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new FindConsoleCommand(view));
		menu.add(new DeleteItemConsoleCommand(view));
		menu.add(new ExecuteConsoleCommand(view));
		menu.execute();
	}

	/**
	 * Виконується при запуску програми
	 * 
	 * @Param args параметри запуску програми
	 */
	public static void main(String[] args) {

		Main menu = new Main();
		menu.run();

	}

}
