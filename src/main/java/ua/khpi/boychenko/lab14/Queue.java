package ua.khpi.boychenko.lab14;

import ua.khpi.boychenko.lab13.Command;

/** Являє
* Методи для приміщення
* І вилучення завдань
* Оброблювачем потоку;
* Шаблон Worker Thread
* @Author Бойченко Олександра
* @Version 1.0
* @See Command
*/
public interface Queue {
	/** Додає нову задачу в чергу;
	* Шаблон Worker Thread
	* @Param cmd завдання
	*/
	void put(Command cmd);
	
	/** Видаляє завдання з черги;
	* Шаблон Worker Thread
	* @Return видаляється завдання
	*/
	Command take();
}
