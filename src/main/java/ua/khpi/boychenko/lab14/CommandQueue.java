package ua.khpi.boychenko.lab14;
import java.util.Vector;

import javafx.scene.web.HTMLEditorSkin;
import ua.khpi.boychenko.lab13.Command;


/**
 * Створює обробник Потоку, що виконує Об'єкти з інтерфейсом Command; шаблон
 * Worker Thread
 * 
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See Command
 */
public class CommandQueue implements Queue {

	/** Черга завдань */
	private Vector<Command> tasks;

	/** Прапор очікування */
	private boolean waiting;

	/** Прапор завершення */
	private boolean shutdown;

	/** Встановлює прапор завершення */
	public void shutdown() {
		shutdown = true;
	}

	/**
	 * Ініціалізація {@linkplain CommandQueue # tasks} {@Linkplain CommandQueue
	 * # waiting} {@Linkplain CommandQueue # waiting}; Створює потік для класу
	 * {@linkplain CommandQueue.Worker}
	 */
	public CommandQueue() {
		tasks = new Vector<Command>();
		waiting = false;
		new Thread(new Worker()).start();
	}

	@Override
	public void put(Command r) {
		tasks.add(r);
		if (waiting) {
			synchronized (this) {
				notifyAll();
			}
		}
	}

	@Override
	public Command take() {
		if (tasks.isEmpty()) {
			synchronized (this) {
				waiting = true;
				try {
					wait();
				} catch (InterruptedException ie) {
					waiting = false;
				}
			}
		}
		return (Command) tasks.remove(0);
	}


	public class Worker implements Runnable {
		/**
		 * Витягує з черги Готові до виконання Завдання; шаблон Worker Thread
		 */
		public void run() {
			while (!shutdown) {
				Command r = take();
				r.execute();
			}
		}
	}
}
