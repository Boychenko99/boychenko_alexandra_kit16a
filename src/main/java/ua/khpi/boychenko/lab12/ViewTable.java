package ua.khpi.boychenko.lab12;

import java.util.Formatter;
import java.util.Scanner;
import java.util.ArrayList;
import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.ViewResult;
import  java.awt.List.*;
/**
 * ConcreteProduct (Шаблон проектування Factory Method) <br>
 * Висновок у вигляді таблиці
 * 
 * @Author Бойченко Олександра
 * @Version 1.0
 * @See ViewResult
 */
public class ViewTable extends ViewResult {

	/** Визначає ширину таблиці за замовчуванням */
	private static final int DEFAULT_WIDTH = 10;

	/** Поточна ширина таблиці */
	private int width;

	/** Встановлює {@linkplain ViewTable # width} значенням <b> width </ b> <br>
	* Викликається конструктор суперкласу {@linkplain ViewResult # ViewResult () ViewResult ()}
	* @Param width визначає ширину таблиці
	*/
	public ViewTable() {
		setWidth(this.DEFAULT_WIDTH);
	}

	/** Встановлює {@linkplain ViewTable # width} значенням <b> width </ b> <br>
	* Викликається конструктор суперкласу {@linkplain ViewResult # ViewResult () ViewResult ()}
	* @Param width визначає ширину таблиці
	*/
	public ViewTable(int width) {

		this.width = width;
	}

	/** Повертає значення поля {@linkplain ViewTable # width}
	* @Return поточна ширина таблиці
	*/
	public int getWidth() {
		return width;
	}

	/** Встановлює поле {@linkplain ViewTable # width} значенням <b> width </ b>
	* @Param width нова ширина таблиці
	* @Return задана параметром <b> width </ b> ширина таблиці
	*/
	public void setWidth(int width) {
		this.width = width;
	}

	/** Виводить горизонтальний роздільник шириною {@linkplain ViewTable # width} символів */
	private void outLine() {
		StringBuilder str = new StringBuilder();
		for (int i = width; i > 0; i--) {
			str.append("-");

		}
		System.out.println(str);
	}

	/** Викликає {@linkplain ViewTable # outLine ()}; завершує висновок роздільником рядків */
	public void outLineLn() {
		outLine();
		System.out.println();
	}

	/**
	* Виводить заголовок таблиці з шириною {@linkplain ViewTable # width} символів
	*/
	private void outHeader() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%2$d%s", "%", (width - 3) / 2, "s | %", "s\n");
		System.out.printf(fmt.toString(), "x ", "y ");
	}

	/** Виводить тіло таблиці з шириною {@linkplain ViewTable # width} символів */
	private void outBody() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%2$d%s", "%", (width - 3) / 2, "s | %", "s\n");
		for (Item item : getItems()) System.out.printf(fmt.toString(), item.getX(), item.getY());
	}

	/** Перевантаження методу суперкласу;
	* Встановлює поле {@linkplain ViewTable # width} значенням <b> width </ b> <br>
	* Для об'єкта {@linkplain ViewTable} викликає метод {@linkplain ViewTable # init (int
	x)}
	* @Param x передається методу <b> init (int) </ b>
	* @Param width нова ширина таблиці.
	*/
	final public void Init(int x, int width) {
		this.width = width;
		Init(x);
	}

	/** Перевизначення (заміщення, overriding) методу суперкласу;
	* Викликає метод {@linkplain ViewResult # Init () Init ()}
	* @Param x аргумент функціі
	*/
	public void Init(int x) {

		int parameter=0;
		
		while(true){
			System.out.println("Input width");
			Scanner in = new Scanner(System.in);
			parameter=in.nextInt();
			
			if(parameter>4 && parameter<50){
				this.width=parameter;
				break;
			}
			
			System.out.println("Incorrect value");
		}
		
		System.out.println("Initialization...");
		super.Init(x);
		System.out.println("Done!");
	}

	/** Перевизначення (заміщення,overriding) методу суперкласу;
	* Зображення елемента таблиці <br>
	* {@InheritDoc}
	*/
	@Override
	public void viewHeader() {
		outHeader();
		outLineLn();
	}

	/** Заміщення (overriding)) методу суперкласу;
	* Зображення елемента таблиці <br>
	* {@InheritDoc}
	*/
	@Override
	public void viewBody() {
		outBody();
	}

	/** Заміщення (overriding)) методу суперкласу;
	* Зображення елемента таблиці <br>
	* {@InheritDoc}
	*/
	@Override
	public void viewFooter() {
		outLineLn();
	}

}
