import static org.junit.Assert.*;

import org.junit.Test;

import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab15.Items;
import ua.khpi.boychenko.lab15.ItemsGenerator;
import ua.khpi.boychenko.lab15.ItemsSorter;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Collections;

import java.util.List;
import java.util.Random;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Тестування розроблених класів
 * 
 * @author Бойченко Олександра
 * @version 6.0
 * @see ItemsGenerator
 * @see ItemsSorter
 * @see Items
 */
public class MainTest5 {
	/** Кількість про елементів в колекції */
	private static final int ITEMS_SIZE = 1000;
	/** Cпостерігач; шаблон Observer */
	private static ItemsGenerator generator = new ItemsGenerator();
	/** Cпостерігач; шаблон Observer */
	private static ItemsSorter sorter = new ItemsSorter();
	/** Cпостережуваний об'єкт; шаблон Observer */
	private static Items observable = new Items();

	/** Виконується першим */
	@BeforeClass
	public static void setUpBeforeClass() {
		observable.addObserver(generator);
		observable.addObserver(sorter);
	}

	/** Тестує операцію додавання об'єктів в колекцію */
	@Test
	public void testAdd() {
		observable.getItems().clear();
		observable.add(new Item("AAA"));
		observable.add("AAA");
		observable.add("");
		observable.add(ITEMS_SIZE);
		for (Item item : observable) {
			assertFalse(item.getData().isEmpty());
		}
		assertEquals(ITEMS_SIZE + 3, observable.getItems().size());
	}

	/** Тестірует операції додавання і видалення об'єктів */
	@Test
	public void testAddDel() {
		Item tmp;
		observable.getItems().clear();
		observable.add("");
		observable.add(ITEMS_SIZE);
		for (int i = ITEMS_SIZE; i > 0; i--) {
			tmp = observable.getItems().get((new Random()).nextInt(i));
			observable.del(tmp);
		}
		assertEquals(1, observable.getItems().size());
	}

	/** Тестує операцію сортування об'єктів */
	@Test
	public void testSort() {
		observable.getItems().clear();
		observable.add(ITEMS_SIZE);
		List<Item> items = new ArrayList<Item>(observable.getItems());
		Collections.sort(items);
		assertEquals(items, observable.getItems());
	}
}