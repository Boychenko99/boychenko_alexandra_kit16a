import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import ua.khpi.boychenko.lab11.Item;
import ua.khpi.boychenko.lab11.View;
import ua.khpi.boychenko.lab11.ViewResult;
import ua.khpi.boychenko.lab13.FindConsoleCommand;
import ua.khpi.boychenko.lab11.ViewResult;
import ua.khpi.boychenko.lab14.AvgCommand;
import ua.khpi.boychenko.lab14.CommandQueue;
import ua.khpi.boychenko.lab14.MaxCommand;
import ua.khpi.boychenko.lab14.MinCommand;

/**
 * Тестування Розроблених класів
 * 
 * @Author Бойченко Олександра
 * @Version 5.0
 * @See CommandQueue
 * @See MaxCommand
 * @See AvgCommand
 * @See MinCommand
 */
public class MainTest4 {

	private final static int N = 1000;
	private static ViewResult view = new ViewResult();
	private static MaxCommand max1 = new MaxCommand(view);
	private static MaxCommand max2 = new MaxCommand(view);
	private static AvgCommand avg1 = new AvgCommand(view);
	private static AvgCommand avg2 = new AvgCommand(view);
	private static MinCommand min1 = new MinCommand(view);
	private static MinCommand min2 = new MinCommand(view);
	private CommandQueue queue = new CommandQueue();

	/** Виконується першим */
	@BeforeClass
	public static void setUpBeforeClass() {
		view.viewInit(5);
		view.viewInit(7);
		view.viewInit(10);
		assertEquals(3, view.getItems().size());
	}

	/** Виконується останнім */
	@AfterClass
	public static void tearDownAfterClass() {
		assertEquals(max1.getResult(), max2.getResult());
		assertEquals(min1.getResult(), min2.getResult());
	}

	/** Перевірка основної функціональності класу {@linkplain MaxCommand} */
	@Test
	public void testMax() {
		max1.execute();
		assertTrue(max1.getResult() == 2);
	}

	/** Перевірка основної функціональності класу {@linkplain AvgCommand} */
	@Test
	public void testAvg() {
		avg1.execute();
		assertTrue(avg1.getResult() == 2.000000);
	}

	/** Перевірка основної функціональності класу {@linkplain MinCommand} */
	@Test
	public void testMin() {
		min1.execute();
		assertTrue(min1.getResult() == 0);
		assertTrue(min1.getResult() == 0);
	}

	/**
	 * Перевірка основної функціональності класу {@linkplain CommandQueue} с
	 * завданням {@linkplain MaxCommand}
	 */
	@Test
	public void testMaxQueue() {
		queue.put(max2);
		try {
			while (max2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			queue.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}

	/**
	 * Перевірка основної функціональності класу {@linkplain CommandQueue} с
	 * завданням {@linkplain AvgCommand}
	 */
	@Test
	public void testAvgQueue() {
		queue.put(avg2);
		try {
			while (avg2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			queue.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}

	/**
	 * Перевірка основної функціональності класу {@linkplain CommandQueue} с
	 * завданням {@linkplain MinCommand}
	 */
	@Test
	public void testMinQueue() {

		queue.put(min2);
		try {
			while (min2.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			queue.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			fail(e.toString());
		}
	}

}
