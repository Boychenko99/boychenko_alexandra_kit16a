package test;

import org.junit.Test;
import junit.framework.Assert;
import ua.khpi.boychenko.lab10.Calc;

import static org.junit.Assert.assertEquals;
import java.io.IOException;

/**
 * Виконує тестування розроблених класів.
 *
 * @author Бойченко Александра
 * @version 1.0
 */
public class MainTest {
    /** Перевірка основної функціональності класу {@linkplain Calc} */
    @Test
    public void testCalc() {
        Calc testObject = new Calc();
        int x = 0;

        x = 149;
        int result = 0;
        int correctResult = 1;
        result = testObject.initialization(x);
        assertEquals(correctResult, result);

        x = 5;
        result = 0;
        correctResult = 2;
        result = testObject.initialization(x);
        assertEquals(correctResult, result);
    }

    /** Перевірка серілізації. Корректності відновленних даних. */
    @Test
    public void testRestore() {
        Calc calc = new Calc();

        int correctResult = 2;
        calc.initialization(5);
        try {
            calc.save();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        calc.initialization(391);
        try {
            calc.restore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        assertEquals(correctResult, calc.getResult().getY());

    }
}
